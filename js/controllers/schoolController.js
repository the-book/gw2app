gw2AppMod.controller('schoolCtrl', ['$scope', 'schoolFcr', function ($scope, schoolFcr) {

    $scope.displayResults = false;
    "use strict";
    $scope.schoolModel = {};
    $scope.filter = {text: ''};
    $scope.selectionFilter = -1;
    $scope.selectedFilter = 'all';

    var init = function () {
        schoolFcr.getStudentStaffAsync(function(results) {
            angular.forEach(results, function(item) {
                if( !item.is_student ) {
                    item.grade = '';
                };
            });
        schoolFcr.getStudentAsync(function(results) {
            angular.forEach(results, function (item) {
                if (item.is_student) {
                    item.grade = '';
                }
            });
            $scope.schoolModel = results;
            $scope.displayResults = true;
        });
    };

    init();

}]);