(function() {
    'use strict';

    gw2AppMod.controller('mapCtrl',['$scope','gw2ApiSvc', function($scope, gw2ApiSvc) {

        var map;

        function init (){
            var southWest, northEast, zoomTool;
            $scope.mapSymbols = [
                { name:"Dungeon", icon: "./content/images/icons/Dungeon_(map_icon).png" },
                { name:"Waypoint", icon: "./content/images/icons/Waypoint_(map_icon).png" },
                { name:"Renown Heart", icon: "./content/images/icons/Incomplete_heart_(map_icon).png" },
                { name:"Skillpoint", icon: "./content/images/icons/Skill_point_empty.png" },
                { name:"Point of Interest", icon: "./content/images/icons/Point_of_interest_(undiscovered).png" },
                { name:"Vista", icon: "./content/images/icons/Vista_empty.png" }
            ];
            map = gw2ApiSvc.generateMap();
            southWest = gw2ApiSvc.unproject(map, [0, 32768]);
            northEast = gw2ApiSvc.unproject(map, [32768, 0]);
            zoomTool = L.control.zoom({position: 'bottomleft'});
            map.setMaxBounds(new L.LatLngBounds(southWest, northEast));
            map.addControl(zoomTool);
            gw2ApiSvc.generateLayer(map, 3, 7, true);
            gw2ApiSvc.drawPOI(map);
        };
        init();

        function onMapClick(e) {
            console.log("You clicked the map at " + map.project(e.latlng));
        }

        map.on('click', onMapClick);

        $( ".panel-legend" )
            .draggable(
            { handle: "div.legend-heading" },
            { containment: "#map" },
            { start: function( event, ui ) {
                console.log("start")
            }
            },
            { stop: function( event, ui ) {
                console.log("stop")
            }
            }
        )

    }]);
})();