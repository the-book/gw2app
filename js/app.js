'use strict';

var gw2AppMod = angular.module('gw2App', ['ngRoute', 'ngAnimate', 'schoolFilters'])

    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/home.html',
                controller: 'homeCtrl'
            })
            .when('/map', {
                templateUrl: 'views/map.html',
                controller: 'mapCtrl'
            })
            .when('/school', {
                templateUrl: 'views/school.html',
                controller: 'schoolCtrl'
            })
            .otherwise('/', {
                templateUrl: 'views/home.html',
                controller: 'homeCtrl'
            });

    });