gw2AppMod.service('gw2ApiSvc', function () {

    this.generateMap = function () {
        return L.map("map", {minZoom: 3, maxZoom: 7, crs: L.CRS.Simple, zoomControl: false}).setView([0,0], 3);
    };

    this.generateLayer = function (map, minZoom, maxZoom, continuousWorld) {
        L.tileLayer("https://tiles.guildwars2.com/1/1/{z}/{x}/{y}.jpg", {
            minZoom: minZoom,
            maxZoom: maxZoom,
            continuousWorld: continuousWorld
        }).addTo(map);
    };

    this.drawPOI = function (map) {

        var unproject = function (coord) {
            return map.unproject(coord, map.getMaxZoom());
        };

        var generateIcon = function () {
            return L.icon({
                iconUrl: "./content/images/icons/Point_of_interest.png",
                iconSize: [20, 20],
                iconAnchor: [10,10],
                popupAnchor: [10,40]
            });
        };

        $.getJSON("https://api.guildwars2.com/v1/map_floor.json?continent_id=1&floor=1", function (data) {
            var region, gameMap, i, il, poi;

            for (region in data.regions) {
                region = data.regions[region];

                for (gameMap in region.maps) {
                    gameMap = region.maps[gameMap];

                    for (i = 0, il = gameMap.points_of_interest.length; i < il; i++) {
                        poi = gameMap.points_of_interest[i];

                        if (poi.type != "waypoint") {
                            continue;
                        }
                        var iconPOI = L.icon({
                            iconUrl: "./content/images/icons/Point_of_interest.png",
                            iconSize: [20, 20],
                            iconAnchor: [10,10],
                            popupAnchor: [10,40]
                        });

                        L.marker(unproject(poi.coord), {
                            title: poi.name,
                            icon: iconPOI
                        }).addTo(map);
                    }
                }
            }
        });
    };

    this.unproject = function (map, coord) {
        return map.unproject(coord, map.getMaxZoom());
    };

    this.genIcon = function (iconUrl, iconSize, iconAnchor, popupAnchor) {
        return L.icon({
            iconUrl: iconUrl,
            iconSize: iconSize, // size of the icon eg. [20, 20]
            iconAnchor: iconAnchor, // point of the icon which will correspond to marker's location eg. [10, 10] (center)
            popupAnchor: popupAnchor // point from which the popup should open relative to the iconAnchor eg. [10,40] (center above)
        });
    };

});