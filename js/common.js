$(document).ready(function () {
    if ($(window).width() < 768) {
        $(".navbar-nav li a").click(function (event) {
            $(".navbar-collapse").collapse('hide');
        });
    }
});

$(window).resize(function () {
    if ($(window).width() < 768) {
        $(".navbar-nav li a").click(function (event) {
            $(".navbar-collapse").collapse('hide');
        });
    }
    else {
        $(".navbar-nav li a").click();
    }
});