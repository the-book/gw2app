gw2AppMod.directive('navigation', function() {
    return {
        restrict: 'A',
        templateUrl: '/GW2API/templates/navigation.html'
    }
});