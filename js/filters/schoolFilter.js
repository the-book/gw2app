angular.module('schoolFilters', []).filter('searchStudentStaff', function () {
    "use strict";
    return function (items, searchText, type) {
        var filtered = [];
        searchText = searchText.toLowerCase();
        angular.forEach(items, function (item) {
            if (item.is_student === type) {
                if ((item.last_name + ', ' + item.first_name).toLowerCase().indexOf(searchText) >= 0 || (item.last_name + ' ' + item.first_name).toLowerCase().indexOf(searchText) >= 0) {
                    filtered.push(item);
                }
            } else if (type === -1) {
                if ((item.first_name + ' ' + item.last_name).toLowerCase().indexOf(searchText) >= 0 || (item.last_name + ' ' + item.first_name).toLowerCase().indexOf(searchText) >= 0) {
                    filtered.push(item);
                }
            }
        });
        return filtered;
    };
});